/**
 * Calculates factorial of given <code>value</code>.
 *
 * @param value positive number
 * @return factorial of <code>value</code>
 */

import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {
        System.out.println(factorial(100));
    }

    public static BigInteger factorial(int value) {
        BigInteger result = BigInteger.valueOf(1);
        for (int i = 0; i < value; i++) {
            result = result.multiply(BigInteger.valueOf(i + 1));
        }
        return result; // your implementation here
    }
}

