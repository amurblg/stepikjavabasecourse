import java.util.Arrays;

/**
 * Merges two given sorted arrays into one
 *
 * @param a1 first sorted array
 * @param a2 second sorted array
 * @return new array containing all elements from a1 and a2, sorted
 */

public class Main {
    public static void main(String[] args) {
        //int[] a = {0, 2, 2}, b = {1, 3};
        //int[] a = {6}, b = {1, 3, 5};
        //int[] a = {}, b = {};
        int[] a = {1, 2, 3}, b = {11, 12, 13};
        System.out.println(Arrays.toString(mergeArrays(a, b)));
    }

    public static int[] mergeArrays(int[] a1, int[] a2) {
        int[] result = new int[a1.length + a2.length];
        int currA1 = 0, currA2 = 0, currRes = 0;
        while (currRes < result.length) {
            if (currA1 == a1.length) {
                result[currRes] = a2[currA2++];
            } else if (currA2 == a2.length) {
                result[currRes] = a1[currA1++];
            } else if (a1[currA1] <= a2[currA2]) {
                result[currRes] = a1[currA1++];
            } else {
                result[currRes] = a2[currA2++];
            }
            currRes++;
        }

        return result; // your implementation here
    }

    public static String magic(String s) {
        if (s.length() == 1) return s;
        String a = s.substring(0, s.length() / 2);
        String b = s.substring(s.length() / 2, s.length());
        return magic(b) + magic(a);
    }
}