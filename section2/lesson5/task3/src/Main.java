import static java.lang.System.*;

public class Main {
    public static void main(String[] args) {
        String[] roles = {"Городничий", "Аммос Федорович", "Артемий Филиппович", "Лука Лукич"};
        String[] textLines = {"Городничий: Я пригласил вас, господа, с тем, чтобы сообщить вам пренеприятное известие: к нам едет ревизор.",
                "Аммос Федорович: Как ревизор?",
                "Артемий Филиппович: Как ревизор?",
                "Городничий: Ревизор из Петербурга, инкогнито. И еще с секретным предписаньем.",
                "Аммос Федорович: Вот те на!",
                "Артемий Филиппович: Вот не было заботы, так подай!",
                "Лука Лукич: Господи боже! еще и с секретным предписаньем!"};

        out.println(printTextPerRole(roles, textLines));
    }

    private static String printTextPerRole(String[] roles, String[] textLines) {
        StringBuilder[] rolesReplicas = new StringBuilder[roles.length];
        for (int i = 0; i < rolesReplicas.length; i++) {
            rolesReplicas[i] = new StringBuilder("");
        }

        for (int i = 0; i < textLines.length; i++) {
            for (int j = 0; j < roles.length; j++) {
                if (textLines[i].startsWith(roles[j] + ":")) {
                    rolesReplicas[j].append(String.valueOf(i + 1) + ")" + textLines[i].substring(roles[j].length() + 1) + "\n");
                    break;
                }
            }
        }

        StringBuilder result = new StringBuilder("");
        for (int i = 0; i < roles.length; i++) {
            result.append(roles[i] + ":\n" + rolesReplicas[i].toString() + "\n");
        }

        return result.toString().trim();
    }
}