/**
 * Checks if given <code>text</code> is a palindrome.
 */

//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        System.out.println(isPalindrome("Madam, i'm Adam!"));
    }

    public static boolean isPalindrome(String text) {
        String clearedString = text.replaceAll("[^0-9a-zA-Z]", "");
        String reversedString =new StringBuilder(clearedString).reverse().toString();
        return reversedString.equalsIgnoreCase(text.replaceAll("[^0-9a-zA-Z]", ""));
    }
}