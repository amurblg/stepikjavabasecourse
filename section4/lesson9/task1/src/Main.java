public class Main {
    public static double sqrt(double x) {
        if (x < 0)
            throw new IllegalArgumentException("Expected non-negative number, got " + String.valueOf(x));
        else
            return Math.sqrt(x); // your implementation here
    }

    public static void main(String[] args) {
        System.out.println(sqrt(2));
    }
}