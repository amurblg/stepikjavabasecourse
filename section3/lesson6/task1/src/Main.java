import jdk.nashorn.internal.ir.WhileNode;

public class Main {
    public static void main(String[] args) {
        Robot vally = new Robot(Direction.LEFT, 0, 0);
        moveRobot(vally, 3, 2);
    }

    public static void moveRobot(Robot robot, int toX, int toY) {
        boolean needToMove = true;
        Direction desiredDir = null;
        while (needToMove) {
            if (robot.getX() > toX) {
                desiredDir = Direction.LEFT;
            } else if (robot.getX() < toX) {
                desiredDir = Direction.RIGHT;
            } else {
                if (robot.getY() > toY) {
                    desiredDir = Direction.DOWN;
                } else if (robot.getY() < toY) {
                    desiredDir = Direction.UP;
                } else {
                    needToMove = false;
                }
            }
            if (needToMove) {
                while (robot.getDirection() != desiredDir) {
                    robot.turnLeft();
                }
                robot.stepForward();
            }
        }
    }
}