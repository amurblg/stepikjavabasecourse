import com.sun.javafx.font.DisposerRecord;

public class Robot {
    private Direction direction;
    private int x;
    private int y;

    public Robot(Direction direction, int x, int y) {
        this.direction = direction;
        this.x = x;
        this.y = y;
    }

    public Direction getDirection() {
        // текущее направление взгляда
        return direction;
    }

    public int getX() {
        // текущая координата X
        return x;
    }

    public int getY() {
        // текущая координата Y
        return y;
    }

    public void turnLeft() {
        // повернуться на 90 градусов против часовой стрелки
        direction = Direction.values()[(direction.ordinal() - 1 < 0 ? 3 : direction.ordinal() - 1)];
    }

    public void turnRight() {
        // повернуться на 90 градусов по часовой стрелке
        direction = Direction.values()[(direction.ordinal() + 1 > 3 ? 0 : direction.ordinal() + 1)];
    }

    public void stepForward() {
        // шаг в направлении взгляда
        // за один шаг робот изменяет одну свою координату на единицу
        switch (direction) {
            case UP:
                y++;
                break;
            case RIGHT:
                x++;
                break;
            case DOWN:
                y--;
                break;
            case LEFT:
                x--;
                break;
        }
    }

    public String toString() {
        return new StringBuilder("direction: ").append(direction.name()).append("; x=").append(x).append("; y=").append(y).append(".").toString();
    }
}
