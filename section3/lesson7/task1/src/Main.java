import java.math.BigDecimal;

public class Main{
    public static void main(String[] args) {
        Timer t = new Timer();
        System.out.println(t.measureTime(new Runnable() {
            @Override
            public void run() {
                new BigDecimal("1234567").pow(100000);
            }
        }));
    }
}