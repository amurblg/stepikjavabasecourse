public class Main {
    public static void main(String[] args) {
        System.out.println(checkLabels(new TextAnalyzer[] {new SpamAnalyzer(new String[]{"розы"}), new NegativeTextAnalyzer(), new TooLongTextAnalyzer(12)}, "А роза упала на лапу Азора :)"));
    }

    public static Label checkLabels(TextAnalyzer[] analyzers, String text) {
        for (TextAnalyzer ta : analyzers) {
            Label result = ta.processText(text);
            if (result != Label.OK)
                return result;
        }

        return Label.OK;
    }
}

enum Label {
    SPAM, NEGATIVE_TEXT, TOO_LONG, OK
}

interface TextAnalyzer {
    Label processText(String text);
}

class SpamAnalyzer extends KeywordAnalyzer {
    private String[] keywords;

    public SpamAnalyzer(String[] keywords) {
        this.keywords = keywords;
    }

    @Override
    protected String[] getKeywords() {
        return keywords;
    }

    @Override
    protected Label getLabel() {
        return Label.SPAM;
    }
}

class NegativeTextAnalyzer extends KeywordAnalyzer {
    @Override
    protected String[] getKeywords() {
        return new String[]{"=(", ":(", ":|"};
    }

    @Override
    protected Label getLabel() {
        return Label.NEGATIVE_TEXT;
    }
}

class TooLongTextAnalyzer implements TextAnalyzer {
    private int maxLength;

    public TooLongTextAnalyzer(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public Label processText(String text) {
        return (text.length() > maxLength ? Label.TOO_LONG : Label.OK);
    }
}

abstract class KeywordAnalyzer implements TextAnalyzer {
    abstract protected String[] getKeywords();

    abstract protected Label getLabel();

    @Override
    public Label processText(String text) {
        Label result = Label.OK;
        String[] keywords = getKeywords();

        for (int i = 0; i < keywords.length; i++) {
            if (text.contains(keywords[i])) {
                result = getLabel();
                break;
            }
        }

        return result;
    }
}