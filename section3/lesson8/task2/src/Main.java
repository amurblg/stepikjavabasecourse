import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        CharSequence acs = new AsciiCharSequence(new byte[]{34, 35, 36});
        System.out.println(acs.subSequence(1, 3));
    }
}

class AsciiCharSequence implements java.lang.CharSequence {
    @Override
    public int length() {
        return charSequence.length;
    }

    @Override
    public String toString() {
        if (charSequence.length == 0)
            return "";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < charSequence.length; i++) {
            sb.append((char) charSequence[i]);
        }
        return sb.toString();
    }

    private byte[] charSequence;

    public AsciiCharSequence(byte[] charSequence) {
        this.charSequence = charSequence;
    }

    @Override
    public char charAt(int index) {
        if (charSequence.length == 0)
            return 0;
        return (char) charSequence[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        if (charSequence.length == 0) return new AsciiCharSequence(new byte[]{});

        byte[] result = new byte[end - start];

        for (int i = start; i < end; i++) {
            result[i - start] = charSequence[i];
        }

        return new AsciiCharSequence(result);
    }
    // implementation
}